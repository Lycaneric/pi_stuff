import time
import RPi.GPIO as GPIO
import servo1 as servo

GPIO.setmode(GPIO.BCM)

led = 4
ser = 20
button = 21
sleep_time = .1

GPIO.setup(led, GPIO.OUT)
GPIO.setup(ser, GPIO.OUT)
GPIO.setup(button, GPIO.IN, pull_up_down=GPIO.PUD_UP)
try:
    while True:
        inputValue = GPIO.input(button)
        if (inputValue == False):
            print("Button Pressed")
            GPIO.output(led, True)
            servo.SetAngle(95)
            servo.SetAngle(30)
        else:
            GPIO.output(led, False)
        time.sleep(sleep_time)
finally:
    GPIO.output(led, False)
    GPIO.cleanup()
