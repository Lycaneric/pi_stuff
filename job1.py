from crontab import CronTab

cron = CronTab(user='pi')
job1 = cron.new(command='python3 /home/pi/projects/pi_stuff/servo1.py')
job1.minute.every(1)

for item in cron:
	print(item)

cron.write()
