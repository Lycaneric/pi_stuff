import servo1 as servo
import RPi.GPIO as GPIO


def ring_gong(channel):
    servo.SetAngle(95)
    servo.SetAngle(30)


GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(10, GPIO.RISING, callback=ring_gong)

message = input("Press enter to quit\n\n")
GPIO.cleanup()
