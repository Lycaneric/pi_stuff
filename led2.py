import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

servo_a = 4
button = 17
sleep_time = .01

GPIO.setup(servo_a, GPIO.OUT)
GPIO.setup(button, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#initialise a previous input variable to 0 (assume button not pressed last)
prev_input = 0
while True:
  #take a reading
  input = GPIO.input(button)
  #if the last reading was low and this one high, print
  if ((not prev_input) and input):
    print("Button pressed")
    GPIO.output(servo_a, True)
    GPIO.output(servo_a, False)
  #update previous input
  prev_input = input
  #slight pause to debounce
  time.sleep(0.05)



#btn_input = GPIO.input(button)

#while True:
#    if btn_input:
#        GPIO.output(led, True)






#try:
#    while True:
#        GPIO.output(led, GPIO.input(button))
#        time.sleep(sleep_time)
#finally:
#    GPIO.output(led, False)
#    GPIO.cleanup()
