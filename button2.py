import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

led = 21
button = 17

GPIO.setup(button, GPIO.IN)
GPIO.setup(led, GPIO.OUT)

prev_input = 0

while True:
    input = GPIO.input(button)

    if ((not prev_input) and input):
        GPIO.output(led, True)
    prev_input = input
    time.sleep(0.05)
